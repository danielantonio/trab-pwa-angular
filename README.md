# Trabalho 2 de Programação Web Avançada

## Alunos :
- Alberto Gusmão Cabral Junior, R.A : 0056119
- Daniel Antônio de Sá, R.A : 0023648
- Gabriel Fernandes Gondim, R.A : 0027967

## Dependências utilizadas

---
### API Servidor de Nomes

Instalar o JSON Server:

**npm i json-server**

Após a instalação, rodar o servidor:

**npm run server**

### Estão sendo utilizadas Bibliotecas do PrimeNG

Instale-as utilizando `npm install`

---

# Versão do Angular : 14.0.4
