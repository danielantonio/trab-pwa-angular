import { Component, OnInit } from '@angular/core';
import { ServicoListaNomesService } from "../services/servico-lista-nomes.service";
import { Nomes } from "./nomes";
import {Router} from "@angular/router";

@Component({
  selector: 'app-componente-lista-nomes',
  templateUrl: './componente-lista-nomes.component.html',
  styleUrls: ['./componente-lista-nomes.component.css']
})
export class ComponenteListaNomesComponent implements OnInit {

  constructor(
    private servicoListaNomes: ServicoListaNomesService,
    private router: Router
  ) { }

  nomes: Nomes[] = []

  ngOnInit(): void {
    this.servicoListaNomes.getNomes().subscribe((nomes) => this.nomes = nomes)
  }

  addNome(nome: string):void {
    this.servicoListaNomes.addNome({nome} as Nomes).subscribe((nome) => {this.nomes.push(nome)});
  }

  excluir(nome: Nomes):void {
    this.nomes = this.nomes.filter(n => n!== nome)
    this.servicoListaNomes.excluirNome(nome.id).subscribe()
  }

  goEditar(nome: Nomes):void{
    this.router.navigate(['/editar-nome'], {queryParams: {id: nome.id,nome: nome.nome}});
  }

}