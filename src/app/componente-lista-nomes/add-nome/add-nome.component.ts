import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-add-nome',
  templateUrl: './add-nome.component.html',
  styleUrls: ['./add-nome.component.css']
})
export class AddNomeComponent implements OnInit {
  form = new FormGroup({
    nome: new FormControl('')
  });

  @Output() addNomeEvento: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  onAddNome():void{
    const nome = this.form.get('nome')?.value;
    if(nome)
      this.addNomeEvento.emit(nome);
    else
      alert("Digite alguma coisa !");
  }

}
