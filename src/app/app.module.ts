import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from "@angular/forms";
import {ToolbarModule} from 'primeng/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ExercicioQuatroComponent } from './exercicio-quatro/exercicio-quatro.component';
import { ComponenteListaNomesComponent } from './componente-lista-nomes/componente-lista-nomes.component';
import {HttpClientModule} from "@angular/common/http";
import { AddNomeComponent } from './componente-lista-nomes/add-nome/add-nome.component';
import { MenuComponent } from './menu/menu.component';
import {MenuModule} from 'primeng/menu';
import { MenubarModule } from 'primeng/menubar';
import {PanelMenuModule} from "primeng/panelmenu";
import {MegaMenuModule} from "primeng/megamenu";
import {ConfirmPopupModule} from 'primeng/confirmpopup';
import {ConfirmationService} from 'primeng/api';
import { PaginaInicialComponent } from './pagina-inicial/pagina-inicial.component';
import { ImagensComponent } from './imagens/imagens.component';
import {ButtonModule} from "primeng/button";
import {ImageModule} from "primeng/image";
@NgModule({
  declarations: [
    AppComponent,
    ComponenteListaNomesComponent,
    AddNomeComponent,
    MenuComponent,
    PaginaInicialComponent,
    ImagensComponent,
    ExercicioQuatroComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    ToolbarModule,
    MenuModule,
    MenubarModule,
    BrowserAnimationsModule,
    PanelMenuModule,
    MegaMenuModule,
    ButtonModule,
    ImageModule,
    ConfirmPopupModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
