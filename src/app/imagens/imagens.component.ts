import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-imagens',
  templateUrl: './imagens.component.html',
  styleUrls: ['./imagens.component.css']
})
export class ImagensComponent implements OnInit {
  form = new FormGroup({
    valor: new FormControl('')
  });
  urls: string []= [];

  constructor() { }

  ngOnInit(): void {
  }

  removeImagem() {
    const url = this.form.get('valor')?.value;
    if(url){
      this.urls = this.urls.filter((item) => {
        return item !== url;
      });
    }
  }

  addImagem(){
    const url = this.form.get('valor')?.value;
    if (url) {
      this.urls.push(url);
    }
  }
}
