import { Injectable } from '@angular/core';
import { Nomes } from "../componente-lista-nomes/nomes";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import {catchError, Observable, of, tap} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class ServicoListaNomesService {

  constructor(private http: HttpClient) { }

  httpOptions = { //permite a aplicação identificar que é um JSON (auxiliar no incremento automatico do ID)
    headers: new HttpHeaders({'Content-Type' : 'application/json'})
  };

  private apiUrl = "http://localhost:3000/nomes";

  getNomes(): Observable<Nomes[]>{

      //Obtendo os dados do servidor via requisição HTTP
      return this.http.get<Nomes[]>(this.apiUrl).pipe(
        tap(_ => console.log("Get de Nomes")),
        catchError(this.handleError<Nomes[]>('getNomes',[]))
      )
  }

  addNome(nome: Nomes): Observable<Nomes>{

    return this.http.post<Nomes>(this.apiUrl,nome,this.httpOptions).pipe(
        tap(_=> console.log(`Add Nome ${nome.nome}`)),
        catchError(this.handleError<Nomes>('addNome'))
    )

  }

  excluirNome(id: number): Observable<any>{

      return this.http.delete<Nomes>(this.apiUrl + `/${id}`,this.httpOptions).pipe(
        tap(_=> console.log(`Nome deletado id=${id}`)),
        catchError(this.handleError<Nomes>('deleteNome'))
      )

  }

  editarNome(nome: Nomes): Observable<Nomes>{
    return this.http.put<Nomes>(this.apiUrl + `/${nome.id}`,nome).pipe(
      tap(_=> console.log(`Editar Nome ${nome.nome}`)),
      catchError(this.handleError<Nomes>('editNome'))
    )
  }

  private handleError<T>(operation = 'operation', result?: T) {

    // retorna um resultado aceitável para a aplicação continuar rodando (um tipo Nomes[])
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };

  }

}
