import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MenuComponent} from "./menu/menu.component";
import {PaginaInicialComponent} from "./pagina-inicial/pagina-inicial.component";
import {ImagensComponent} from "./imagens/imagens.component";
import {ComponenteListaNomesComponent} from "./componente-lista-nomes/componente-lista-nomes.component";
import {ExercicioQuatroComponent} from "./exercicio-quatro/exercicio-quatro.component";

const routes: Routes = [
  {
    path: "",
    component: PaginaInicialComponent
  },
  {
    path: "imagens",
    component: ImagensComponent
  },
  {
    path: "add-nome",
    component: ComponenteListaNomesComponent
  },
  {
    path: "editar-nome",
    component: ExercicioQuatroComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
