import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExercicioQuatroComponent } from './exercicio-quatro.component';

describe('ExercicioQuatroComponent', () => {
  let component: ExercicioQuatroComponent;
  let fixture: ComponentFixture<ExercicioQuatroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExercicioQuatroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExercicioQuatroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
