import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {ConfirmationService} from 'primeng/api';
import {ServicoListaNomesService} from "../services/servico-lista-nomes.service";
import {Nomes} from "../componente-lista-nomes/nomes";
import {Location} from "@angular/common";

@Component({
  selector: 'app-exercicio-quatro',
  templateUrl: './exercicio-quatro.component.html',
  styleUrls: ['./exercicio-quatro.component.css'],
  providers: [ConfirmationService]
})
export class ExercicioQuatroComponent implements OnInit {

  form: FormGroup = this.fb.group({
    id: [],
    nome: []
  });

  model: Nomes = new class implements Nomes {
    id: number;
    nome: string;
  }

  @Output() retornarValorSelecionado: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private confirmationService: ConfirmationService,
              private servicoListaNomes: ServicoListaNomesService,
              private location: Location) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.form.get('id')?.setValue(params['id']);
      this.form.get('nome')?.setValue(params['nome']);
    });
  }

  confirm(event: Event) {
    this.confirmationService.confirm({
      target: event.target ? event.target : undefined,
      message: 'Deseja Editar o nome?',
      acceptLabel: 'Sim',
      rejectLabel: 'Não',
      accept: () => {
        this.model.id = this.form.controls['id'].value;
        this.model.nome = this.form.controls['nome'].value
        this.servicoListaNomes.editarNome(this.model).subscribe()
        this.location.back();
      }
    });
  }


}
